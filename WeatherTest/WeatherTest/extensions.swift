//
//  extensions.swift
//  WeatherTest
//
//  Created by Benoît Durand on 01/09/2018.
//  Copyright © 2018 Benoît Durand. All rights reserved.
//

import UIKit

extension UILabel {
    convenience init(size: CGFloat? = 15.0, color: UIColor? = .black, lines: Int = 0, alignment: NSTextAlignment = .left) {
        self.init()
        self.textColor = color
        self.font = UIFont.systemFont(ofSize: size ?? 15.0)
        self.numberOfLines = lines
        self.textAlignment = alignment
    }
}

extension String {
    
    // MARK: Localization
    func localized(args: [Any]? = nil) -> String {
        var localized = NSLocalizedString(self.lowercased(), comment: "")
        
        args?.forEach({ arg in
            if let range = localized.range(of: "$var") {
                localized.replaceSubrange(range, with: "\(arg)")
            }
        })
        
        return localized.lowercased() != self ? localized : "localization not found <\(localized)>"
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension UIImageView {
    func setImageWithFade(image: UIImage) {
        UIView.transition(with: self,
                          duration:0.5,
                          options: .transitionCrossDissolve,
                          animations: { self.image = image },
                          completion: nil)
    }
}

extension UISearchBar {
    
    private func getViewElement<T>(type: T.Type) -> T? {
        
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    func setTextFieldColor(color: UIColor) {
        
        if let textField = getViewElement(type: UITextField.self) {
            switch searchBarStyle {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 6
                
            case .prominent, .default:
                textField.backgroundColor = color
            }
        }
    }
}

extension UIStackView {
    func addArrangedSubviews(_ views: [UIView]) {
        for i in 0..<views.count {
            addArrangedSubview(views[i])
        }
    }
}

extension UIView {
    
    convenience init(backgroundColor: UIColor) {
        self.init()
        self.backgroundColor = backgroundColor
    }
    
    var cornerRadius: CGFloat? {
        get { return layer.cornerRadius }
        set {
            layer.cornerRadius = newValue ?? 0
            layer.masksToBounds = (newValue ?? CGFloat(0.0)) > CGFloat(0.0)
        }
    }
    
    func fadeIn(withDuration: TimeInterval = 0.2) {
        self.isHidden = false
        UIView.animate(withDuration: withDuration) {
            self.alpha = 1
        }
    }
    
    func fadeOut(withDuration: TimeInterval = 0.2) {
        UIView.animate(withDuration: withDuration, animations: {
            self.alpha = 0
        }) { (done) in
            self.isHidden = true
        }
    }
    
    func addSubviews(_ views: [UIView]) {
        for i in 0..<views.count {
            addSubview(views[i])
        }
    }
    
    func setParallax(amount: Int) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        self.addMotionEffect(group)
    }
    
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionFade)
    }
    
}

extension Date {
    func hoursFromMidnight(hours: Int) -> Bool {
        let calendar = Calendar.current
        
        var dateComp = DateComponents()
        dateComp.hour = hours

        let selfComponents = calendar.dateComponents([.hour, .minute], from: self)
        if let newDate = calendar.date(byAdding: selfComponents, to: Date(timeIntervalSince1970: 0)) {
            return Calendar.current.date(byAdding: dateComp, to: Date(timeIntervalSince1970: 0)) == newDate
        } else {
            return false
        }
    }
    
    func string(withFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Calendar.current.locale
        formatter.timeZone = Calendar.current.timeZone
        return formatter.string(from: self)
    }

}


extension UIViewController {
    
    func showError(body: String) {
        let alert = UIAlertController(title: "error/title".localized(), message: body, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
}
