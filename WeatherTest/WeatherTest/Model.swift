//
//  Model.swift
//  WeatherTest
//
//  Created by Benoît Durand on 03/09/2018.
//  Copyright © 2018 Benoît Durand. All rights reserved.
//

import UIKit

struct ForecastData: Codable {
    
    let list : [Forecast?]
    
    private enum CodingKeys: String, CodingKey {
        case list
    }
    
    struct Forecast: Codable {
        let date: Date?
        let weather: [Weather?]
        let wind: Wind?
        let infos: Infos?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            weather = try values.decode(Array<Weather?>.self, forKey: .weather)
            wind = try values.decode(Wind.self, forKey: .wind)
            infos = try values.decode(Infos.self, forKey: .infos)
            let dateDouble = try values.decode(Double.self, forKey: .date)
            date = Date(timeIntervalSince1970: dateDouble)

        }
        
        private enum CodingKeys: String, CodingKey {
            case date = "dt"
            case weather
            case wind
            case infos = "main"
        }
        
    }
}

struct WeatherData: Codable {
    
    let name: String?
    let weather: [Weather?]
    let wind: Wind?
    let id: Int
    let infos: Infos?
    
    
    private enum CodingKeys: String, CodingKey {
        case name
        case id
        case weather
        case wind
        case infos = "main"
    }
    
    
    

}


struct RequestError: Codable {
    let code: String?
    let message: String?
    
    private enum CodingKeys: String, CodingKey {
        case code = "cod"
        case message
    }
    
    var messageToDisplay: String {
        switch code {
        case "404":
            return "error/invalidcity".localized()
        default:
            return "error/unknown".localized() + "\n\(message ?? "")"
        }
    }
}

struct Weather: Codable {
    var description: String?
    var icon: String?
    var id: Int?
    var main: String?
    
    func getDescription() -> String {
        switch id ?? 0 {
        case 200..<300:
            return "weather/code.2xx".localized()
        case 300..<500:
            return "weather/code.3xx".localized()
        case 500, 501:
            return "weather/code.\(id ?? 555)".localized()
        case 500..<600:
            return "weather/code.502-531".localized()
        case 700..<800:
            return "weather/code.7xx".localized()
        case 800..<900:
            return "weather/code.\(id ?? 804)".localized()
        default:
            return ""
        }
    }
}


struct Wind: Codable {
    var speed: Float?
    var deg: Double?
}


struct Infos: Codable {
    var temperature: Float?
    var maxTemperature: Float?
    var minTemperature: Float?
    var temperatureFelt: Float?
    var humidity: Float?
    
    private enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case maxTemperature = "temp_max"
        case minTemperature = "temp_min"
        case temperatureFelt = "temp_kf"
        case humidity
    }
}

