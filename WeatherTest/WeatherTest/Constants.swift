//
//  Constants.swift
//  WeatherTest
//
//  Created by Benoît Durand on 02/09/2018.
//  Copyright © 2018 Benoît Durand. All rights reserved.
//

import UIKit

struct Constants {
    static let weatherApiKey = "" // Set OpenWeatherAPIKey
    static let openWeatherBaseURL = "http://api.openweathermap.org/data/2.5/"
}
