//
//  ForecastTableViewCell.swift
//  WeatherTest
//
//  Created by Benoît Durand on 04/09/2018.
//  Copyright © 2018 Benoît Durand. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var minMaxLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(background: UIImage, date: String, temperature: Float, min: Float, max: Float, description: String) {
        self.backgroundImage.image = background
        self.dateLabel.text = date
        self.temperatureLabel.text = "\(temperature)°c"
        self.minMaxLabel.text = "forecast/minmax".localized(args: [min, max])
        self.descriptionLabel.text = description
    }
    
}
