//
//  ViewController.swift
//  WeatherTest
//
//  Created by Benoît Durand on 01/09/2018.
//  Copyright © 2018 Benoît Durand. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit

class HomeViewController: UIViewController {
    
    // MARK: Layout properties
    
    fileprivate var searchTitle = UILabel(size: 18.0, color: .white)
    fileprivate var searchBar = UISearchBar()
    fileprivate var background = UIImageView(image: #imageLiteral(resourceName: "background"))
    fileprivate var isFirstSearch: Bool = true
    fileprivate var cityLabel = UILabel(size: 30.0, color: .white)
    fileprivate var temperatureLabel = UILabel(size: 45.0, color: .white)
    fileprivate var dateLabel = UILabel(size: 25.0, color: .white, alignment: .center)
    fileprivate var weatherDescription = UILabel(size: 25, color: .white)
    fileprivate var additionalData = UILabel(size: 15, color: .white)
    fileprivate var windLabel = UILabel(size: 15, color: .white, lines: 1)
    fileprivate var currentWeatherContainer = UIView()
    fileprivate var forecastWeatherContainer = UIView(backgroundColor: UIColor.white.withAlphaComponent(0.2))
    fileprivate var pageSelector = PageSelector(leftTitle: "home/pagecontrol.current".localized(), rightTitle: "home/pagecontrol.week".localized(), color: .white, selectedColor: .black)

    
    // MARK: Properties
    
    fileprivate var forecastTableViewController = ForecastTableViewController()
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.addSubviews([background, searchBar, searchTitle])
        background.contentMode = .scaleAspectFill
        background.setParallax(amount: 40)
        searchBar.placeholder = "home/search.placeholder".localized()
        searchBar.searchBarStyle = .minimal
        searchBar.setTextFieldColor(color: UIColor.white.withAlphaComponent(0.8))
        searchBar.delegate = self
        searchTitle.text = "home/search.title".localized()
        pageSelector.cornerRadius = 8
        pageSelector.didChangeValue = switchPage
        pageSelector.isHidden = true
        currentWeatherContainer.isHidden = true
        forecastWeatherContainer.isHidden = true
        
        self.buildCurrentweather()
        
        
        // Constraints
        
        
        background.snp.makeConstraints({ cm in
            cm.edges.equalToSuperview().inset(-100)
        })
        
        searchBar.snp.makeConstraints({ cm in
            cm.center.equalToSuperview()
            cm.left.right.equalToSuperview().inset(16)
        })
        
        searchTitle.snp.makeConstraints({ cm in
            cm.bottom.equalTo(searchBar.snp.top)
            cm.centerX.equalToSuperview()
        })
    }
    
    func setCurrentWeather(data: WeatherData) {
        self.view.fadeTransition(0.3)
        self.cityLabel.text = data.name ?? ""
        self.temperatureLabel.text = "\(data.infos?.temperature ?? 0.0)°c"
        self.additionalData.text = "current/additional".localized(args: [data.infos?.humidity ?? 0])
        dateLabel.text = Date().string(withFormat: "EEEE dd MMM")
        if let weather = data.weather.first, let icon = weather?.icon{
            self.background.image = UIImage(named: icon.lowercased())
            self.weatherDescription.text = weather?.getDescription()
            self.windLabel.text = "current/wind".localized(args: [data.wind?.speed ?? 0.0, String(format: "%.0f", data.wind?.deg ?? 0.0)])
        }
        
    }
    
    func switchPage(index: Int) {
        if index == 0 {
            forecastWeatherContainer.fadeOut()
        } else {
            forecastWeatherContainer.fadeIn()
        }
    }
    
    func initWeatherView() {
        isFirstSearch = false
        self.searchTitle.fadeOut()
        self.searchTitle.removeFromSuperview()
        self.view.layoutIfNeeded()
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        self.forecastWeatherContainer.addSubviews([blurEffectView, self.forecastTableViewController.view])
        self.forecastTableViewController.tableView.tableFooterView = UIView(backgroundColor: .clear)
        self.addChildViewController(self.forecastTableViewController)
        self.view.addSubviews([currentWeatherContainer, forecastWeatherContainer, pageSelector])
        self.view.bringSubview(toFront: searchBar)

        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: {
            self.searchBar.snp.remakeConstraints({ cm in
                if #available(iOS 11.0, *) {
                    cm.left.top.right.equalTo(self.view.safeAreaLayoutGuide).inset(20)
                } else {
                    cm.left.top.right.equalToSuperview().inset(20)
                }
            })
            
            
            
            self.pageSelector.snp.makeConstraints({ cm in
                cm.top.equalTo(self.searchBar.snp.bottom).offset(15)
                cm.left.right.equalToSuperview().inset(30)
            })
            
            self.currentWeatherContainer.snp.makeConstraints({ cm in
                cm.top.equalTo(self.pageSelector.snp.bottom).offset(20)
                cm.left.right.bottom.equalToSuperview()
            })
            
            blurEffectView.snp.makeConstraints({ cm in
                cm.edges.equalToSuperview()
            })
            
            self.forecastTableViewController.view.snp.makeConstraints({ cm in
                cm.left.right.bottom.equalToSuperview()
                cm.top.equalTo(self.pageSelector.snp.bottom).offset(15)
            })
            
            self.forecastWeatherContainer.snp.makeConstraints({ cm in
                cm.edges.equalToSuperview()
            })
            
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.currentWeatherContainer.fadeIn()
            self.pageSelector.fadeIn()
        })
        
    }
    
    func buildCurrentweather() {
        let windImage = UIImageView(image: #imageLiteral(resourceName: "wind"))
        let stack = UIStackView()
        windImage.tintColor = .white
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 4.0
        stack.addArrangedSubviews([windImage, windLabel])
        currentWeatherContainer.addSubviews([cityLabel, weatherDescription, temperatureLabel, dateLabel, stack, additionalData])
        
        windImage.snp.makeConstraints({ cm in
            cm.size.equalTo(20)
        })
        
        temperatureLabel.snp.makeConstraints({ cm in
            cm.center.equalToSuperview()
        })
        
        dateLabel.snp.makeConstraints({ cm in
            cm.top.equalTo(temperatureLabel.snp.bottom).offset(5)
            cm.centerX.equalToSuperview()
            cm.left.right.equalToSuperview()
        })
        
        weatherDescription.snp.makeConstraints({ cm in
            cm.bottom.equalTo(temperatureLabel.snp.top)
            cm.centerX.equalToSuperview()
        })
        
        cityLabel.snp.makeConstraints({ cm in
            cm.bottom.equalTo(weatherDescription.snp.top)
            cm.centerX.equalToSuperview()
        })
        
        stack.snp.makeConstraints({ cm in
            cm.left.bottom.equalToSuperview().inset(15)
        })
        
        additionalData.snp.makeConstraints({ cm in
            cm.left.equalToSuperview().inset(15)
            cm.bottom.equalTo(stack.snp.top).offset(-4)
        })

        
    }
    
    func getWeather(city: String) {
        let request = "\(Constants.openWeatherBaseURL)weather?APPID=\(Constants.weatherApiKey)&q=\(city)&units=metric"
        
        if let weatherRequestURL = URL(string: request) {
            Alamofire.request(weatherRequestURL).responseJSON { response in
                if let data = response.data {
                    if let error = response.error {
                        self.showError(body: error.localizedDescription)
                    }
                    do {
                        let decoder = JSONDecoder()
                        if let currentWeather = try? decoder.decode(WeatherData.self, from: data) {
                            if self.isFirstSearch {
                                self.initWeatherView()
                            }
                            self.view.layoutIfNeeded()
                            self.setCurrentWeather(data: currentWeather)
                        } else {
                            let requestError = try decoder.decode(RequestError.self, from: data)
                            self.showError(body: requestError.messageToDisplay)
                        }
                    } catch  {
                        self.showError(body: "error/unknown".localized())
                    }
                }
                
            }
        }
    }
    
    func getForecast(city: String) {
        let request = "\(Constants.openWeatherBaseURL)forecast?APPID=\(Constants.weatherApiKey)&q=\(city)&units=metric"
        
        if let forecastRequestURL = URL(string: request) {
            Alamofire.request(forecastRequestURL).responseJSON { response in
                if let data = response.data {
                    if let error = response.error {
                        self.showError(body: error.localizedDescription)
                    }
                    do {
                        let decoder = JSONDecoder()
                        if let currentWeather = try? decoder.decode(ForecastData.self, from: data) {
                            self.forecastTableViewController.setData(data: currentWeather.list)
                        } else {
                            let requestError = try decoder.decode(RequestError.self, from: data)
                            self.showError(body: requestError.messageToDisplay)
                        }
                    } catch  {
                        self.showError(body: "error/unknown".localized())
                    }
                }
                
            }
        }
    }

}

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, !text.isEmpty {
            searchBar.resignFirstResponder() 
            getWeather(city: text)
            getForecast(city: text)
        }
    }
}


