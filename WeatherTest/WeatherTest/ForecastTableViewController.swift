//
//  ForecastTableViewController.swift
//  WeatherTest
//
//  Created by Benoît Durand on 04/09/2018.
//  Copyright © 2018 Benoît Durand. All rights reserved.
//

import UIKit

class ForecastTableViewController: UITableViewController {

    fileprivate var forecast: [ForecastData.Forecast?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: String(describing: ForecastTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ForecastTableViewCell.self))
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        tableView.backgroundColor = .clear
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.count
    }

    func setData(data: [ForecastData.Forecast?]) {
        self.forecast = data
        self.forecast = data.filter({ $0?.date?.hoursFromMidnight(hours: 14) ?? false })

        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ForecastTableViewCell.self), for: indexPath)
        if let weather = forecast[indexPath.row]?.weather.first, let icon = weather?.icon, let date = forecast[indexPath.row]?.date, let infos = forecast[indexPath.row]?.infos {
            (cell as? ForecastTableViewCell)?.setData(background: UIImage(named: icon.lowercased()) ?? UIImage(),
                                                      date: date.string(withFormat: "EEE dd/MM").capitalizingFirstLetter(),
                                                      temperature: infos.temperature ?? 0.0,
                                                      min: infos.minTemperature ?? 0.0,
                                                      max: infos.maxTemperature ?? 0.0,
                                                      description: weather?.getDescription() ?? "")
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }


}
