//
//  PageSelector.swift
//  WeatherTest
//
//  Created by Benoît Durand on 03/09/2018.
//  Copyright © 2018 Benoît Durand. All rights reserved.
//

import UIKit

class PageSelector: UIView {
    
    var didChangeValue: ((Int) -> ())?
    
    enum PageSelectorStyle {
        case none, tracking, underlined
    }
    
    fileprivate var stack = UIStackView()
    fileprivate var leftButton = UIButton()
    fileprivate var rightButton = UIButton()
    var selected = 0
    fileprivate var selectedColor = UIColor.black
    fileprivate var color = UIColor.white
    fileprivate var style = PageSelectorStyle.none
    fileprivate var tracking = UIView(backgroundColor: .white)
    fileprivate var leftUnderline = UIView()
    fileprivate var rightUnderLine = UIView()
    
    init(leftTitle: String, rightTitle: String, color: UIColor, selectedColor: UIColor) {
        super.init(frame: .zero)

        self.backgroundColor = UIColor.clear
        tracking.cornerRadius = 8
        self.addSubview(tracking)
        
        tracking.snp.makeConstraints({ cm in
            cm.left.top.bottom.equalToSuperview()
            cm.width.equalTo( self.snp.width).dividedBy(2)
        })
        
        stack.distribution = .fillEqually

        
        self.addSubview(stack)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
        stack.alignment = .fill
        stack.spacing = 15
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        
        leftButton.setTitle(leftTitle, for: .normal)
        leftButton.tag = 0
        leftButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
        leftButton.setTitleColor(selectedColor, for: .selected)
        leftButton.setTitleColor(selectedColor, for: .highlighted)
        leftButton.setTitleColor(color, for: .normal)
        leftButton.contentHorizontalAlignment = .center
        leftButton.isSelected = true
        rightButton.tag = 1
        rightButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
        rightButton.setTitle(rightTitle, for: .normal)
        rightButton.setTitleColor(selectedColor, for: .selected)
        rightButton.setTitleColor(selectedColor, for: .highlighted)
        rightButton.setTitleColor(color, for: .normal)
        rightButton.contentHorizontalAlignment = .center

        stack.addArrangedSubviews([leftButton, rightButton])
        
        
        stack.snp.makeConstraints({ cm in
            cm.edges.equalToSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func buttonSelected(sender: UIButton) {
        if selected != sender.tag {
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseOut], animations: {
                self.tracking.snp.remakeConstraints({ cm in
                    if sender.tag == 1 {
                        cm.right.top.bottom.equalToSuperview()
                    } else {
                        cm.left.top.bottom.equalToSuperview()
                    }
                    cm.width.equalTo( self.snp.width).dividedBy(2)
                })
                self.layoutIfNeeded()
            })
        }
        

        selected = sender.tag
        leftButton.isSelected = selected == 0
        rightButton.isSelected = selected == 1
        didChangeValue?(sender.tag)
    }

    
}
