
## How to run


1. Clone
2. Pod install
3. Open WeatherTest.xcworkspace
4. Fill weatherApiKey in WeatherTest/Constants.swift with your own OpenWeatherApiKey
5. Run the app

## To run the app on a device
6. please select a team in project settings -> General -> Signing -> Team
7. change bundle id to a unique string example : com.myTeam.WeatherTest 






